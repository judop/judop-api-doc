# Judop REST API v1.0

Judop is an e-commerce platform where you can make your own website. And this is an API for Judop platforms. You can create order, product etc with this API and also list your products in marketplaces.

- [Judop REST API](#judop-rest-api)
    - [API Information](#api-information)
    - [Product](#product)
    - [Order](#order)
    - [Category](#category)
    - [Brand](#brand)
    - [Feature](#feature)
    - [Unit](#unit)
    - [Variant](#variant)
    - [Location](#location)
    - [Enums](#enums)
    

## API Information

|  Schema | Host  | Prefix
|---|---|---|
| https | Domain of Judop client panel | /api/v1/

> **All endpoints and descriptions**

| Method | Endpoint      | Description                       |
|:-----------| :--------------|:----------------------------------|
| `GET` | `/product`      | Returns a list of all product, [detail](#product-list)
| `POST` | `/product/create` | Create a new product, [detail](#product-create)
| `PUT` | `/product/update` | Update existing product, [detail](#product-update)
| `PUT` | `/product/update/state` | Update multiple or single product state, [detail](#product-update-state)
| `POST` | `/order` | Returns a list of all orders, [detail](#order-list)
| `POST` | `/order/create` | Create a new order, [detail](#order-create)
| `PUT` | `/order/update-state` | Update existing order state, [detail](#order-update-state)
| `GET` | `/category` | Returns a list of all category, [detail](#category-list)
| `POST` | `/category/create` | Create a new category, [detail](#category-create)
| `PUT` | `/category/update` | Update existing category, [detail](#category-update)
| `GET` | `/brand` | Returns a list of all brand, [detail](#brand-list)
| `POST` | `/brand/create` | Create a new brand, [detail](#brand-create)
| `POST` | `/brand/update` | Update existing brand, [detail](#brand-update)
| `GET` | `/feature` | Returns a list of all feature, [detail](#feature-list)
| `POST` | `/feature/create` | Create a new feature, [detail](#feature-create)
| `PUT` | `/feature/update` | Update existing feature, [detail](#feature-update)
| `GET` | `/unit` | Returns a list of all unit, [detail](#unit-list)
| `POST` | `/unit/create` | Create a new unit, [detail](#unit-create)
| `PUT` | `/unit/update` | Update existing unit, [detail](#unit-update)
| `GET` | `/variant` | Returns a list of all variant, [detail](#variant-list)
| `POST` | `/variant/create` | Create a new variant, [detail](#variant-create)
| `PUT` | `/variant/update` | Update existing variant, [detail](#variant-update)
| `GET` | `/location/country` | Get list of countries, [detail](#country-list)
| `GET` | `/location/city` | Get list of cities, [detail](#city-list)
| `GET` | `/location/town` | Get list of towns, [detail](#town-list)

## Product

Everything about your products. You can list all of them, create a new product 
(or multiple products), update existing product or update multiple product 
state with following requests;

>  - [List](#product-list)
>  - [Create](#product-create)
>  - [Update](#product-update)
>  - [Update State](#product-update-state)

### Product List

> **List your products**

Example response:

```
GET /api/v1/product/
```
```json
{
        "Status": "OK",
        "Data": [
            {
                "info": {
                    "name": "test ürün",
                    "slug": "test-urun",
                    "description": "<p>test<\/p>",
                    "summary_description": "test",
                    "installment_enable": true
                },
                "category": {
                    "name": "Bilgisayar",
                    "slug": "bilgisayar"
                },
                "brand": null,
                "vat": {
                    "name": "Kdv18",
                    "slug": "kdv18",
                    "percent": "18",
                    "vat_included": true
                },
                "unit": {
                    "name": "Adet",
                    "slug": "adet",
                    "is_float": false
                },
                "images": [],
                "features": [],
                "variants": [
                    {
                        "parent": {
                            "name": "Renk",
                            "slug": "renk"
                        },
                        "children": [
                            {
                                "id": 2,
                                "name": "Kırmızı",
                                "slug": "kirmizi"
                            },
                            {
                                "id": 4,
                                "name": "Yeşil",
                                "slug": "yesil"
                            }
                        ]
                    },
                    {
                        "parent": {
                            "name": "Beden",
                            "slug": "beden"
                        },
                        "children": [
                            {
                                "id": 7,
                                "name": "Small",
                                "slug": "small"
                            },
                            {
                                "id": 9,
                                "name": "Large",
                                "slug": "large"
                            }
                        ]
                    }
                ],
                "prices": [
                    {
                        "values": [
                            2,
                            9
                        ],
                        "sku": "sssx",
                        "barcode": "sssx",
                        "stock_count": 1,
                        "price_info": {
                            "sale_price": 500,
                            "sale_currency": 0
                        },
                        "discount_info": null
                    },
                    {
                        "values": [
                            2,
                            7
                        ],
                        "sku": "ssst",
                        "barcode": "ssst",
                        "stock_count": 1,
                        "price_info": {
                            "sale_price": 500,
                            "sale_currency": 0
                        },
                        "discount_info": null
                    },
                    {
                        "values": [
                            4,
                            9
                        ],
                        "sku": "sssv",
                        "barcode": "sssv",
                        "stock_count": 1,
                        "price_info": {
                            "sale_price": 500,
                            "sale_currency": 0
                        },
                        "discount_info": null
                    },
                    {
                        "values": [
                            4,
                            7
                        ],
                        "sku": "sssc",
                        "barcode": "sssc",
                        "stock_count": 2,
                        "price_info": {
                            "sale_price": 500,
                            "sale_currency": 0
                        },
                        "discount_info": {
                            "amount": 100,
                            "type": 0,
                            "description": "New Year Discount"
                        }
                    }
                ],
                "seo": {
                    "title": null,
                    "description": null,
                    "keywords": null
                }
            }
        ]
    }
```

### Product Create

> **Create a new product or multiple products**

```
POST /api/v1/product/create/
```
```json
{
    "products": [
        {
            "product_info": {
                "state": 1,
                "name": "Apple Watch SE 44mm",
                "summary_description": "Apple Watch SE 44mm",
                "html_description": "<p><strong>Apple Watch 6 SE 44mm</strong></p>",
                "installment_enable": true
            },
            "category": "bilgisayar",
            "brand": "apple",
            "vat": {
                "slug": "kdv18",
                "included": true
            },
            "unit": {
                "slug": "adet",
                "is_float": false
            },
            "cargo_info": {
                "desi": 1,
                "width": 1,
                "height": 1,
                "length": 1,
                "weight": 1
            },
            "images": [
                {
                    "path": "image_url.png",
                    "is_cover": true
                }
            ],
            "variants": [
                {
                    "parent": "renk",
                    "children": [
                        {
                            "id": 2,
                            "slug": "kirmizi"
                        },
                        {
                            "id": 5,
                            "slug": "mavi"
                        }
                    ]
                }
            ],
            "prices": [
                {
                    "values": [
                        2
                    ],
                    "sku": "testappk",
                    "barcode": "testappk",
                    "stock_count": 1,
                    "price_info": {
                        "sale_price": 3299,
                        "sale_currency": 0,
                        "cost_price": 2650,
                        "cost_currency": 0
                    },
                    "discount_info": null
                },
                {
                    "values": [
                        5
                    ],
                    "sku": "testappm",
                    "barcode": "testappm",
                    "stock_count": 1,
                    "price_info": {
                        "sale_price": 3299,
                        "sale_currency": 0,
                        "cost_price": 2650,
                        "cost_currency": 0
                    },
                    "discount_info": {
                        "amount": 100,
                        "type": 0,
                        "description": "New Year"
                    }
                }
            ],
            "features": [
                {
                    "id": 2,
                    "value": "1GB"
                }
            ]
        }
    ]
}
```

#### Request parameters explanation

Parameter       | IsRequired | Default | Type      | Description
-------------   | -------- | --------- | --------- | --------
products        | Yes      | | array     | Products for create
product_info    | Yes      | | object    | Product informations
product_info.name | Yes | | string | Product name
product_info.summary_description | Yes | | string | Summary description of product
product_info.html_description | Yes | | string | Html description of product
product_info.installment_enable | No | false | boolean | Installment information
category | Yes | | string | Category slug of product, [detail](#category)
brand | No | | string | Brand slug of product, [detail](#brand)
vat | Yes | | object | Vat information of product, [detail](#vat)
vat.slug | Yes | | string | Slug of vat
vat.included | No | true | boolean | Vat included to product price or not
unit | Yes | | object | Unit information of product, [detail](#unit)
unit.slug | Yes | | string | Slug of unit
unit.is_float | No | false | boolean | Product unit can be float or not
cargo_info | Yes | | object | Cargo information of product
cargo_info.desi | Yes | | integer | Desi of product
cargo_info.width | No | | integer | Width of product
cargo_info.height | No | | integer | Height of product
cargo_info.length | No | | integer | Length of product
cargo_info.weight | No | | integer | Weight of product
images | No | | array | Image of product
images.*.path | Yes | | string | Image url
images.*.is_cover | No | false | boolean | Image is cover of product, at least one of the image has to be cover
variants | No | | array | Variants of product, if there is no variant then don't send. [detail](#variant)
variants.*.parent | Yes | | string | Parent slug of variant
variants.*.children | Yes | | array | Child variants
variants.\*.children.*.id | Yes | | int | Variant ID
variants.\*.children.*.slug | Yes | | string | Variant slug
prices | Yes | | array | Prices of product with variant combinations
prices.\*.values | Yes | | array | Variants IDs of product, leave empty if there is no variant in product
prices.\*.sku | Yes | | string | SKU information
prices.\*.barcode | Yes | | string | Barcode information
prices.\*.stock_count | Yes | | int | Stock count
prices.\*.price_info | Yes | | object | Price information of product or variant combinations
prices.\*.price_info.sale_price | Yes | | float | Sale price of product
prices.\*.price_info.sale_currency | Yes | 0 | CurrencyEnum | Product sale price currency, [detail](#currency-enum)
prices.\*.price_info.cost_price | Yes | | float | Cost of product
prices.\*.price_info.cost_currency | Yes | 0 | CurrencyEnum | Product cost currency [detail](#currency-enum)
prices.\*.discount_info | No | null | object | Product discount information
prices.\*.discount_info.amount | Yes? | | float | Discount amount
prices.\*.discount_info.type | Yes? | 0 | PriceDiscountEnum | Discount type [detail](#discount-type-enum)
prices.\*.discount_info.description | No | null | string | Discount description
features | No | | array | Features of product, [detail](#feature)
features.*.id | Yes | | integer | ID of feature
features.*.value | Yes | | string | Value of feature

> :warning: **Question mark in the "IsRequired" column, that means required when object is sent. Otherwise is not required**


### Product Update

> **Update a product information.** 

> :warning: **Can not add new variants in update request payload. That's only allowed in one time.**

> :bulb: **Update request payload is same as [create](#product-create). But it needs one additional parameter;**

Parameter       | IsRequired | Default | Type      | Description
-------------   | -------- | --------- | --------- | --------
product        | Yes      | | string     | Product slug to update

### Product Update State

> **Update multiple or single product state**

Example request;

```
PUT /api/v1/product/update/state/
```
```json
{
    "slugs": [
        "product-1", 
        "product-2"
    ], 
    "state": 1
}
```

Parameter       | IsRequired | Default | Type      | Description
-------------   | -------- | --------- | --------- | --------
slugs        | Yes      | | array     | Product slugs to update
state        | Yes      | | StateEnum     | Product state, [detail](#state-enum)


## Order

Everything about your orders. You can list all of them, create a new order or 
update an order state with following requests;

>  - [List](#order-list)
>  - [Create](#order-create)
>  - [Update State](#order-update-state)

### Order List

> **Order list request can be filtered. Example request payload;**

```
POST /api/v1/order/
```
```json
{
    "filters": {
        "order": "12345678",
        "state": [1], 
        "product": [
            "product-1"
        ], 
        "start": "2021-04-08 15:45",
        "finish": "2021-04-15 16:30",
        "payment": [1]
    }
}
```

Parameter      | Type      | Description
-------------  | --------- | --------
order          | string     | Order number provided from Judop. If this filter sent then other filters will be ignored
state          | array(OrderStateEnum) | Orders with state, [detail](#order-state-enum)
product        | array | Orders containing these product slug
start | DateTime | Orders after this date
finish | DateTime | Orders before this date
payment | array(PaymentTypeEnum) | Orders with payment type, [detail](#payment-type-enum)

<br/>

> **Example response;**

```json
{
  "Status": "OK",
  "Data": [
    {
      "info": {
        "order_no": "0U9GEDQ0FC",
        "state": 1,
        "cargo_company": "MNG",
        "cargo_track_number": null,
        "cargo_price": 8,
        "cargo_commission": 0,
        "created_at": "2021-11-03 20:23:00"
      },
      "items": [
        {
          "slug": "product-test",
          "name": "Product Test",
          "variant": "Siyah",
          "sku": "test-s",
          "barcode": "test-s",
          "quantity": 1,
          "unit": "Adet",
          "price": 9840,
          "discount": 0,
          "vat_percent": 18,
          "subtotal": 9840,
          "currency": 0,
          "image": "https://www.image.com/test_image.png",
          "cancelled": false,
          "reason_of_cancel": null
        }
      ],
      "payment_info": {
        "payment_type": 0,
        "currency": 0,
        "provision_number": "xxxxxxxxx",
        "payment_tries": []
      },
      "merchant": {
        "merchant_no": "08317071",
        "name": "XXXX YYYY",
        "phone": "5554443322",
        "email": "test@hotmail.com"
      },
      "billing_address": {
        "name": "XXXX YYYY",
        "phone": "5554443322",
        "address": "Test address",
        "town": "Ataşehir",
        "town_id": 450,
        "city": "İSTANBUL",
        "city_id": 34,
        "country": "Türkiye",
        "country_id": 212
      },
      "shipping_address": {
        "name": "XXXX YYYY",
        "phone": "5554443322",
        "address": "Test address",
        "town": "Ataşehir",
        "town_id": 450,
        "city": "İSTANBUL",
        "city_id": 34,
        "country": "Türkiye",
        "country_id": 212
      },
      "order_total": {
        "subtotal": 9840,
        "discount": 0,
        "vat": 2160,
        "total": 12000,
        "cancelled": 0,
        "currency": 0
      }
    }
  ]
}
```



### Order Create

> **Create a new order**

```
POST /api/v1/order/create/
```
```json
{
    "merchant":{
        "no": "12121212", 
        "email": "test@gmail.com", 
        "phone": "5554443322", 
        "name": "XXXX YYYY"
    }, 
    "affiliate": 1, 
    "state": 0, 
    "order_date": "03-11-2021 20:23", 
    "state_update_date": "03-11-2021 20:23", 
    "payment_type": 1, 
    "provision_number": "xxxxxxxxx", 
    "installment_rate": 0, 
    "installment_interest": 0, 
    "installment_count": 1, 
    "description": "Test description", 
    "cargo_track_number": null, 
    "cargo_company": "MNG", 
    "cargo_price": 8, 
    "cargo_commission": 0, 
    "items": [
        {
            "product": "product-1", 
            "unit": "Adet", 
            "desi": 5, 
            "quantity": 1, 
            "variants": [
                {
                    "slug": "kirmizi", 
                    "name": "Kırmızı"
                }
            ], 
            "price_info": {
                "subtotal": 9840, 
                "discount": 0, 
                "vat": 2160, 
                "vat_percent": 18,
                "currency": 0, 
                "vat_included": true
            }
        }
    ], 
    "shipping_address": {
        "name": "XXXX YYYY", 
        "phone": "5554443322", 
        "address": "Test address", 
        "town": 913, 
        "city": 61, 
        "country": 212
    }, 
    "billing_address": {
        "name": "XXXX YYYY", 
        "phone": "5554443322", 
        "address": "Test address", 
        "town": 450, 
        "city": 34, 
        "country": 212
    }
}
```

#### Request parameters explanation

Parameter       | IsRequired | Default | Type      | Description
-------------   | -------- | --------- | --------- | --------
merchant        | Yes      | | object     | Merchant information
merchant.no        | No      | null | string     | Merchant number in Judop
merchant.email | Yes | | string | Email address of merchant
merchant.phone | Yes | | string | Mobile phone number of merchant
merchant.name | Yes | | string | First and last name of merchant
affiliate | Yes | | AffiliateEnum | ID of marketplace, [detail](#affiliate-enum)
state | Yes | | OrderStateEnum | Order state, [detail](#order-state-enum)
order_date | Yes |  | DateTime | Order created at date
state_update_date | Yes |  | DateTime | Last updated at date of order state
payment_type | Yes | | PaymentTypeEnum | Order payment type, [detail](#payment-type-enum)
provision_number | No | null | string | Provision number of payment
installment_rate | Yes |  | float | Installment rate of payment
installment_interest | Yes |  | float | Installment interest of payment
installment_count | Yes |  | integer | Installment count of payment
description | No | null | string | Description of order
cargo_track_number | No | null | string | Track number of order
cargo_company | Yes | | CargoCompanyEnum | Cargo company of order, [detail](#cargo-company-enum)
cargo_price | Yes | | float | Cargo price
cargo_commission | Yes | | float | Cargo commission
items | Yes | | array | Items in order
items.*.product | Yes | | string | Product slug of item
items.*.unit | No | | string | Unit slug of product
items.*.desi | Yes | | integer | Desi of product
items.*.quantity | Yes | | integer/float | Quantity of product, can be float if product unit has "is_float" flag
items.*.variants | No | | array | Variant information of product, if product has variants
items.\*.variants.*.slug | Yes | | string | Variant slug
items.\*.variants.*.name | Yes | | string | Variant name
items.*.price_info | Yes | | object | Item price informations
items.*.price_info.subtotal | Yes | | float | Subtotal of item
items.*.price_info.discount | No | 0 | float | Total discount of item
items.*.price_info.vat | Yes | | float | Total vat of item
items.*.price_info.vat_percent | Yes | | integer | Vat percent
items.*.price_info.vat_included | Yes | | boolean | If vat included
items.*.price_info.currency | Yes | | CurrencyEnum | Currency of item, [detail](#currency-enum)
shipping_address | Yes | | object | Shipping address of order
shipping_address.name | Yes | | string | First and last name of shipping
shipping_address.phone | Yes | | string | Mobile phone number of shipping
shipping_address.address | Yes | | string | Address of shipping
shipping_address.town | Yes | | integer | Town information of shipping, [detail](#town-list)
shipping_address.city | Yes | | integer | City information of shipping, [detail](#city-list)
shipping_address.country | Yes | | integer | Country information of shipping, [detail](#country-list)
billing_address | Yes | | object | Billing address of order
billing_address.name | Yes | | string | First and last name of billing
billing_address.phone | Yes | | string | Mobile phone number of billing
billing_address.address | Yes | | string | Address of billing
billing_address.town | Yes | | integer | Town information of billing, [detail](#town-list)
billing_address.city | Yes | | integer | City information of billing, [detail](#city-list)
billing_address.country | Yes | | integer | Country information of billing, [detail](#country-list)

### Order Update State

```
PUT /api/v1/order/update-state/
```
```json
{
    "order": "ABDF5DF24", 
    "state": 2, 
    "cargo_company": "MNG", 
    "cargo_track_number": "xxxxx"
}
```

Parameter    | IsRequired  | Type      | Description
-------------  | ----------- | --------- | --------
order         | Yes | string     | Order number to update
state         | Yes | OrderStateEnum | New order state, [detail](#order-state-enum)
cargo_company      | Yes? | CargoCompanyEnum | Cargo company, [detail](#cargo-company-enum). Required when state is 5 ("CARGOED", [detail](#state-enum))
cargo_track_number | Yes? | string | Cargo track number. Required when state is 5 ("CARGOED", [detail](#state-enum))

## Category

Everything about your categories. You can list all of them, create a new one or update 
an existing category with following requests;

>  - [List](#category-list)
>  - [Create](#category-create)
>  - [Update](#category-update)

### Category List

> **List your categories. Example response;**

```
GET /api/v1/category/
```
```json
{
    "Status": "OK", 
    "Data": [
        {
            "parent": null, 
            "state": 1, 
            "name": "Bilgisayar", 
            "slug": "bilgisayar", 
            "logo_path": null
        }, 
        {
            "parent": null, 
            "state": 1, 
            "name": "Teknoloji",
            "slug": "teknoloji",
            "logo_path": "www.image.com/technology.jpg"
        },
        {
            "parent": "teknoloji",
            "state": 1,
            "name": "Telefon",
            "slug": "telefon",
            "logo_path": null
        }
    ]
}
```

### Category Create

> **Create a new category**

```
POST /api/v1/category/create/
```
```json
{
    "state": 1,
    "name": "Tablet",
    "parent": "teknoloji",
    "logo_path": "www.image.com/tablet.jpg"
}
```

Parameter       | IsRequired | Default | Type      | Description
-------------   | -------- | --------- | --------- | --------
state        | No      | 1 | StateEnum     | Category state, [detail](#state-enum)
name        | Yes      | | string     | Category name
parent        | No      | null | string     | Parent category slug
logo_path        | No      | null | string     | Category image url

### Category Update

> **Update existing category**

```
PUT /api/v1/category/update/
```
```json
{
    "category": "telefon",
    "state": 1,
    "name": "Tablet 2",
    "parent": "teknoloji",
    "logo_path": "www.image.com/tablet_2.jpg"
}
```

Parameter       | IsRequired | Default | Type      | Description
-------------   | -------- | --------- | --------- | --------
category        | Yes      | | string     | Category to update
state        | No      | 1 | StateEnum     | Category state, [detail](#state-enum)
name        | No      | | string     | Category name
parent        | No      | null | string     | Parent category slug
logo_path        | No      | null | string     | Category image url

## Brand

Everything about your brands. You can list all of them, create a new one or update
an existing brand with following requests;

>  - [List](#brand-list)
>  - [Create](#brand-create)
>  - [Update](#brand-update)

### Brand List

> **List your brands. Example response;**

```
GET /api/v1/brand/
```
```json
{
    "Status": "OK",
    "Data": [
        {
            "state": 1,
            "name": "Lenovo",
            "logo_path": "https://www.image.com/lenovo_logo.png",
            "slug": "lenovo"
        },
        {
            "state": 1,
            "name": "Apple",
            "logo_path": null,
            "slug": "apple"
        },
        {
            "state": 1,
            "name": "Toshiba",
            "logo_path": null,
            "slug": "toshiba"
        }
    ]
}
```

### Brand Create

> **Create a new brand**

```
POST /api/v1/brand/create/
```
```json
{
    "state": 1,
    "name": "Samsung",
    "logo_path": "www.image.com/samsung.png"
}
```

Parameter       | IsRequired | Default | Type      | Description
-------------   | -------- | --------- | --------- | --------
state        | No      | 1 | StateEnum     | Brand state, [detail](#state-enum)
name        | Yes      | | string     | Brand name
logo_path        | No      | null | string     | Brand image url

### Brand Update

> **Update existing brand**

```
PUT /api/v1/brand/update/
```
```json
{
    "brand": "samsung",
    "state": 1,
    "name": "Samsung 2",
    "logo_path": "www.image.com/samsung_2.png"
}
```

Parameter       | IsRequired | Default | Type      | Description
-------------   | -------- | --------- | --------- | --------
brand        | Yes      | | string     | Brand to update
state        | No      | 1 | StateEnum     | Brand state, [detail](#state-enum)
name        | No      | | string     | Brand name
logo_path        | No      | null | string     | Brand image url

## Feature

Everything about your features. You can list all of them, create a new one or update
an existing feature with following requests;

>  - [List](#feature-list)
>  - [Create](#feature-create)
>  - [Update](#feature-update)

### Feature List

> **List your features. Example response;**

```
GET /api/v1/feature/
```
```json
{
    "Status": "OK",
    "Data": [
        {
            "id": 1,
            "state": 1,
            "name": "Hardisk",
            "description": null
        },
        {
            "id": 2,
            "state": 1,
            "name": "RAM",
            "description": null
        }
    ]
}
```

### Feature Create

> **Create a new feature**

```
POST /api/v1/feature/create/
```
```json
{
    "state": 1,
    "name": "CPU"
}
```

Parameter       | IsRequired | Default | Type      | Description
-------------   | -------- | --------- | --------- | --------
state        | No      | 1 | StateEnum     | Feature state, [detail](#state-enum)
name        | Yes      | | string     | Feature name

### Feature Update

> **Update existing brand**

```
PUT /api/v1/feature/update/
```
```json
{
    "feature": 1,
    "name": "Hardisk"
}
```

Parameter       | IsRequired | Default | Type      | Description
-------------   | -------- | --------- | --------- | --------
brand        | Yes      | | string     | Feature to update
name        | Yes      | | string     | Feature name
description | No | null | string | Feature description

## Unit

Everything about your units. You can list all of them, create a new one or update
an existing unit with following requests;

>  - [List](#unit-list)
>  - [Create](#unit-create)
>  - [Update](#unit-update)

### Unit List

> **List your units. Example response;**

```
GET /api/v1/unit/
```
```json
{
    "Status": "OK",
    "Data": [
        {
            "state": 1,
            "name": "Adet",
            "slug": "adet",
            "is_float": false,
            "description": null
        },
        {
            "state": 1,
            "name": "Litre",
            "slug": "litre",
            "is_float": true,
            "description": null
        }
    ]
}
```

### Unit Create

> **Create a new feature**

```
POST /api/v1/unit/create/
```
```json
{
    "state": 1,
    "name": "Litre",
    "is_float": true
}
```

Parameter       | IsRequired | Default | Type      | Description
-------------   | -------- | --------- | --------- | --------
state        | No      | 1 | StateEnum     | Unit state, [detail](#state-enum)
name        | Yes      | | string     | Unit name
is_float        | No      | false | boolean     | Unit can be float or not

### Unit Update

> **Update existing brand**

```
PUT /api/v1/unit/update/
```
```json
{
    "unit": "litre",
    "is_float": true
}
```

Parameter       | IsRequired | Default | Type      | Description
-------------   | -------- | --------- | --------- | --------
unit        | Yes      | | string     | Unit to update
is_float        | No      | false | boolean     | Unit can be float or not
description | No | null | string | Unit description

## Variant

Everything about your variants. You can list all of them, create a new one or
update an existing variant with following requests;

>  - [List](#variant-list)
>  - [Create](#variant-create)
>  - [Update](#variant-update)

### Variant List

> **List your variants. Example response;**

```
GET /api/v1/variant/
```
```json
{
    "Status": "OK",
    "Data": [
        {
            "parent": "renk",
            "child": [
                {
                    "state": 1,
                    "name": "Kırmızı",
                    "slug": "kirmizi",
                    "description": null
                },
                {
                    "state": 1,
                    "name": "Siyah",
                    "slug": "siyah",
                    "description": null
                }
            ]
        },
        {
            "parent": "beden",
            "child": [
                {
                    "state": 1,
                    "name": "Small",
                    "slug": "small",
                    "description": null
                },
                {
                    "state": 1,
                    "name": "Medium",
                    "slug": "medium",
                    "description": null
                },
                {
                    "state": 1,
                    "name": "Large",
                    "slug": "large",
                    "description": null
                }
            ]
        }
    ]
}
```

### Variant Create

> **Create a new variant**

```
POST /api/v1/variant/create/
```
```json
{
    "parent": {
        "name": "Numara"
    },
    "variants": [
        {
            "name": "39"
        },
        {
            "name": "40"
        },
        {
            "name": "41"
        },
        {
            "name": "42"
        }
    ]
}
```

Parameter       | IsRequired | Default | Type      | Description
-------------   | -------- | --------- | --------- | --------
parent.name        | Yes      | | string     | Parent variant slug
variants.*.name        | Yes      | | string     | Child variant value

### Variant Update

> **Update existing variant**

```
PUT /api/v1/variant/update/
```
```json
{
    "variant": "41",
    "state": 1,
    "parent": "numara",
    "description": "Test description"
}
```

Parameter       | IsRequired | Default | Type      | Description
-------------   | -------- | --------- | --------- | --------
variant        | Yes      | | string     | Variant to update
state       | No | 1 | StateEnum | Variant state, [detail](#state-enum)
parent      | No | | string | Variant parent
description | No | null | string | Variant description

## Location

You can list country, city and town informations with following requests;

> - [Country List](#country-list)
> - [City List](#city-list)
> - [Town List](#town-list)

### Country List

> **All country informations**

Example response;

```
GET /api/v1/location/country/
```
```json
{
  "Status": "OK",
  "Data": [
    {
      "id": 1,
      "iso2": "AF",
      "iso3": "AFG",
      "name": "Afghanistan",
      "phone_code": "93"
    },
    {
      "id": 2,
      "iso2": "AL",
      "iso3": "ALB",
      "name": "Albania",
      "phone_code": "355"
    },
    ...
  ]
}
``` 

### City List

> **All city informations**

Example response;

```
GET /api/v1/location/city/
```
```json
{
  "Status": "OK",
  "Data": [
    {
      "id": 1,
      "name": "ADANA",
      "country_id": 212
    },
    {
      "id": 2,
      "name": "ADIYAMAN",
      "country_id": 212
    },
    ...
  ]
}
``` 

### Town List

> **All town informations**

Example response;

```
GET /api/v1/location/town/
```
```json
{
  "Status": "OK",
  "Data": [
    {
      "id": 1,
      "name": "Aladağ",
      "city_id": 1
    },
    {
      "id": 2,
      "name": "Ceyhan",
      "city_id": 1
    },
    {
      "id": 3,
      "name": "Çukurova",
      "city_id": 1
    },
    ...
  ]
}
``` 

## Enums

List of all enums and accepted values;

> - [State](#state-enum)
> - [Currency](#currency-enum)
> - [Discount Type](#discount-type-enum)
> - [Order State](#order-state-enum)
> - [Payment Type](#payment-type-enum)
> - [Affiliate](#affiliate-enum)
> - [Cargo Company](#cargo-company-enum)


### State Enum

Name        | Value    
----------- | -------- 
DELETED     | 0        
ACTIVE      | 1        
PASSIVE     | 2        

### Currency Enum

Name        | Value
----------- | -------- 
TL          | 0
USD         | 1
EUR         | 2    

### Discount Type Enum

Name        | Value
----------- | -------- 
AMOUNT      | 0
PERCENT     | 1   

### Order State Enum

Name                 | Value
-------------------- | -------- 
WAITING_FOR_PAYMENT  | 0
WAITING_FOR_APPROVE  | 1
APPROVED             | 2
IN_SUPPLY            | 3
IN_PREPARE           | 4
CARGOED              | 5
CANCELED             | 6
DELIVERED            | 7
RETURNED             | 8
OTHER                | 9

### Payment Type Enum

Name                 | Value
-------------------- | -------- 
CREDIT_CARD          | 0
CASH_ON_DELIVERY     | 1  
CARD_ON_DELIVERY     | 2  
TRANSFER_EFT         | 3  

### Affiliate Enum

Name            | Value
--------------- | -------- 
TRENDYOL        | 1
HEPSIBURADA     | 2
N11             | 3
GITTIGIDIYOR    | 4
AMAZON          | 5
CICEKSEPETI     | 6
PTTAVM          | 7
ALINIYOR        | 8

### Cargo Company Enum

Name     | Value
-------- | -------- 
MNG      | 0
PTT      | 1
ARAS     | 2
SURAT    | 3
YURTICI  | 4
UPS      | 5

